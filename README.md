# Complexity Finder

**Complexity Finder** is a script that reads pairs of input and output
of a specific function from a file and draws a graph of them along with
an estimation of their complexity.

I have come to develop this script for purposes of computer science.
Specifically for estimating the input size - execution time complexity
of algorithms' implementations. This fact doesn't necessarily limit
someone to use this script for mathematics' purposes.

The suggestion of an estimated complexity is done as follows. The
complexities to be analyzed are hardcoded in the script. For every
complexity, given the inputs, are assumed the outputs of a function if
this function had this complexity. Then all the assumptions are compared
to the original outputs and it suggested the assumption that falls
closer to it. Essentially this means that the suggestion can only come
out of a canvas of complexities therefor the script cannot suggest any
complexity possible. There is the potential though for extending this
canvas but as the actual complexity becomes more obscure, it becomes
more unlikely to have a canvas with enough complexities to pick from.
Still this script is a good tool to estimate and visualize the
complexity of a function and familiarize someone with complexities.

### Requirements Dependencies and Downloading

I developed the script on Linux but this doesn't mean it exclusively
runs on Linux; It can probably run on other operating systems. The drill
is that it uses the `sys` library for managing command line arguments
and I don't know how other operating systems would behave with it.

Apart from `python3` it is required that `matplotlib` is also present.

The only way to download this script is through this very webpage using
its utilities and other tools such as `git`.

### Using Complexity Finder

To run the script you must beforehand have acquired a file with the
input and the output of the function (input size and execution time of
the algorithm) that it is to be analyzed. This file must be a text file
containing two columns of either floating point numbers or integers. The
first column is for input and the second for output. These two columns must be
separated with whitespaces. Although most of the times the input column
consists of integers, there are functions that make float point
operations with these numbers. Hence the size of the numbers is bound by
floating point limit set by python.

To run the script you must simply open a terminal, navigate to the
location of the script and run:

```shell
./finder.py <datafile>
```

or if you are using a file system that doesn't support permissions:

```shell
python finder.py <datafile>
```

Then if everything goes as expected, the graph with the `matplotlib`
interface will appear similar to the following example.

![example]

If you desire to extend the canvas of the complexities you must program
the function of the complexity yourself, import it in the code and add
the function in the list of the rest functions that they will be used
for the analysis. This list is referred as _[comp]_ in the code.

### Contributing

Even though this is a personal project, in the _extreme_ case of someone
feeling benevolent enough to spare time for a script as small and
insignificant as this, I would wholeheartedly accept and appreciate any
efforts.

I have some ideas on extending the functionality of the script and I can
reveal them to you through p.m if you wish to know and implement them. I
also accept any thoughts and ideas that anyone could have.

[example]: examples/times_graph.png
[comp]: finder.py#L65
