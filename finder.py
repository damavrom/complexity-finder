#!/usr/bin/python3

import matplotlib.pyplot as plt
import sys
import math

###  Reading datafile  ###

if len(sys.argv) < 2:
    print("filename is required")
    exit()

times = {}
with open(sys.argv[1], "r") as fl:
    for inp in fl:
        i = int(inp.split()[0])
        t = float(inp.split()[1])
        times[i] = t
s = sum(times.values())
start = min(times)
end = max(times)

###  Defining Complexities  ###

# The functions that define the complexities.
# More can be declared.
functions = {}

functions["constant"]     = lambda n: 1
functions["linear"]       = lambda n: n
functions["quadratic"]    = lambda n: n**2
functions["cubic"]        = lambda n: n**3
functions["quartic"]      = lambda n: n**4
functions["logarithmic"]  = lambda n: math.log(n, 2)
functions["nlogarithmic"] = lambda n: n*math.log(n, 2)

###  Suggesting Complexity  ###

min_dist = -1
for f in functions:
    integral = sum(map(lambda n: functions[f](n), times))
    dist = sum(map(lambda n: (functions[f](n)*s/integral-times[n])**2, times))
    if min_dist == -1 or min_dist > dist:
        min_dist = dist
        estim_integ = integral
        estim_func = f

compx = list(range(start, end+1, max(1, int((end-start)/100))))
compy = list(map(lambda n: functions[estim_func](n)*s/estim_integ, compx))

###  Plotting  ###

plt.plot(list(times), list(times.values()), 'ro',
         label='execution time', markersize=2)
plt.plot(compx, compy, label = f"{estim_func} complexity")
plt.legend()
plt.tight_layout()
plt.show()
